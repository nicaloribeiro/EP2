import java.awt.image.BufferedImage;

public class Texturas {
	
	public BufferedImage player, tiro, inimigo;
	private SpriteSheet ss;
	
	public Texturas(Game game){
		ss = new SpriteSheet(game.getSpriteSheet());
		
		getTexturas();
		
		
	}
	private void getTexturas(){
		player = ss.grabImage(1, 1, 32, 32);
		tiro = ss.grabImage(2, 1, 32, 32);
		inimigo = ss.grabImage(3, 1, 32, 32);
		
	}

}
