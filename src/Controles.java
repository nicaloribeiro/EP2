import java.awt.Graphics;
import java.util.LinkedList;
import java.util.Random;

public class Controles {
	private LinkedList<EntityA> ea = new LinkedList<EntityA>();
	private LinkedList<EntityB> eb = new LinkedList<EntityB>();
	
	EntityA enta;
	EntityB entb;
	Texturas tex;
	Random r = new Random();
	
	public Controles(Texturas tex){
		this.tex = tex; 
		//for(int i =0; i<20; i++)
		//addEntity(new Inimigos(r.nextInt(640),10, tex));
	}
	
	
	

	public void criaInimigo(int inimigo_gerado){
		for(int i = 0; i < inimigo_gerado; i++){
			addEntity(new Inimigos(r.nextInt(640), -10, tex));
		}
		
	}
	public void tick(){
		// TIPO A
		for(int i = 0; i < ea.size(); i++){
			enta = ea.get(i);
			enta.tick();
		}
		
		// TIPO B
				for(int i = 0; i < eb.size(); i++){
					entb = eb.get(i);
					entb.tick();
				}
		
	}
	public void render(Graphics g){
		// TIPO A
		for(int i = 0; i < ea.size(); i++){
			enta = ea.get(i);
			enta.render(g);		
	}
		// TIPO B
				for(int i = 0; i < eb.size(); i++){
					entb = eb.get(i);
					entb.render(g);			
			}
}
	public void addEntity(EntityA block){
		ea.add(block);
	}
	public void removeEntity(EntityA block){
		ea.remove(block);
	}
	public void addEntity(EntityB block){
		eb.add(block);
	}
	public void removeEntity(EntityB block){
		eb.remove(block);
	}


	public Texturas getTex() {
		return tex;
	}

	public void setTex(Texturas tex) {
		this.tex = tex;
	}
	public LinkedList<EntityA> getEntityA(){
		return ea;
	}
	public LinkedList<EntityB> getEntityB(){
		return eb;
	}
}