import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class Inimigos extends GameObject implements EntityB{
	
	private Texturas tex;
	Random r = new Random();
	private int velocidade = r.nextInt(4) + 1;
	
	public Inimigos(double x, double y, Texturas tex){
		super(x, y);
		this.tex = tex;		
	}
	
	public void tick(){
		y += velocidade;
		
		if(y > Game.HEIGHT * Game.SCALE){ // VER ISSO
			velocidade = r.nextInt(4) + 1;
			x = r.nextInt(640);
			y = -10;
			}
			
		
	}
	
	public void render(Graphics g){
		g.drawImage(tex.inimigo, (int) x, (int) y, null);
	}
	public Rectangle getBounds(){
		return new Rectangle((int)x, (int)y, 32, 32);
	}
	
	public double getY(){
		return y;
	}
	public void setY(double y){
		this.y = y;
	}
	public double getX(){
		return x;
	}
	public void setX(double x){
		this.x = x;
	}
}
