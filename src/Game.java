import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.LinkedList;

import javax.swing.JFrame;

public class Game extends Canvas implements Runnable  {
	

	private static final long serialVersionUID = 1L;
	public static final int WIDHT = 320;
	public static final int HEIGHT = 260;
	public static final int SCALE = 2;
	public final String TITLE = "EP2 - Space Game";
	
	private boolean running = false;
	private Thread thread;
	
	private BufferedImage image = new BufferedImage(WIDHT,HEIGHT,BufferedImage.TYPE_INT_RGB);
	private BufferedImage spriteSheet = null;
	private BufferedImage background = null;
	private boolean atirando = false;
	private Player p;
	private Controles c;
	private Texturas tex;
	public LinkedList<EntityA> ea;
	public LinkedList<EntityB> eb;
	private int inimigo_gerado = 2;
	private int inimigo_eliminado = 0;
	
	public void init(){
		requestFocus();
		CarregaImagem loader = new CarregaImagem();
		try{
			spriteSheet = loader.carregaImagem("/sprite2.png");
			background = loader.carregaImagem("/background.png");
		}
		catch (IOException e){
			e.printStackTrace();
			}
	
	addKeyListener(new TeclaInput(this));
	
	tex = new Texturas(this);	
	p = new Player(320,480, tex);
	c = new Controles(tex);
	
	ea = c.getEntityA();
	eb = c.getEntityB();
	
	c.criaInimigo(inimigo_gerado);
	}
	private synchronized void start(){
		if(running)
			return;
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	private synchronized void stop(){
		if(!running)
			return;
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.exit(1);
		
		
	}
			
	
	public void run(){
		init();
		long lTime = System.nanoTime();
		final double nFps = 60.0;
		double ns = 1000000000 / nFps;
		double beta = 0;
		int updates = 0;
		int fps = 0;
		long temp = System.currentTimeMillis();
		
				
		while(running){
			
			long nTime = System.nanoTime();
			beta += (nTime - lTime)/ ns;
			lTime = nTime;
			if (beta >= 1) {
				tick();
				updates++;
				beta --;
			}
			render();
			fps++;
			if(System.currentTimeMillis() - temp > 1000) {
				temp +=1000;
				System.out.println(updates + "Ticks, Fps" + fps);
				updates = 0;
				fps = 0;
			}
			
		}
		stop();
		
	}
	
	private void tick(){
		p.tick();
		c.tick();
	}
	 private void render(){
		 BufferStrategy bs = this.getBufferStrategy();
		 if(bs == null){
			 createBufferStrategy(3);
			 return;
		 }
		 Graphics g = bs.getDrawGraphics();
		 
		 g.drawImage(image,0, 0, getWidth(),getHeight(), this);
		 g.drawImage(background, 0, 0, null);
		 p.render(g);
		 c.render(g);
		 g.dispose();
		 bs.show();
	 }
	 
	 public void keyPressed(KeyEvent e){
		 int key = e.getKeyCode();
		 
		 if(key == KeyEvent.VK_RIGHT){
			 p.setVelX(5);			 
		 } else if (key == KeyEvent.VK_LEFT){
			 p.setVelX(-5);	 
		 } else if (key == KeyEvent.VK_DOWN){
			 p.setVelY(5);				 
		 } else if (key == KeyEvent.VK_UP){
			 p.setVelY(-5);				 
		 }else if(key == KeyEvent.VK_SPACE &&  !atirando){
			 atirando = true;
			 c.addEntity(new Bullet(p.getX(), p.getY(), tex, this));
		 }
			
		}
		public void keyReleased(KeyEvent e){
			 int key = e.getKeyCode();
			 
			 if(key == KeyEvent.VK_RIGHT){
				 p.setVelX(0);			 
			 } else if (key == KeyEvent.VK_LEFT){
				 p.setVelX(0);	 
			 } else if (key == KeyEvent.VK_DOWN){
				 p.setVelY(0);				 
			 } else if (key == KeyEvent.VK_UP){
				 p.setVelY(0);				 
			 }else if( key == KeyEvent.VK_SPACE){
				 atirando = false;
			 }
			
		}
	
	public static void main(String args[]){
		Game game = new Game();
		
		game.setPreferredSize(new Dimension(WIDHT * SCALE, HEIGHT * SCALE));
		game.setMaximumSize(new Dimension(WIDHT * SCALE, HEIGHT * SCALE));
		game.setMinimumSize(new Dimension(WIDHT * SCALE, HEIGHT * SCALE));
		
		JFrame frame = new JFrame(game.TITLE);
		frame.add(game);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		game.start();	
 
		
	}
	
	public BufferedImage getSpriteSheet(){
		return spriteSheet;
	}
	public int getInimigo_gerado() {
		return inimigo_gerado;
	}
	public void setInimigo_gerado(int inimigo_gerado) {
		this.inimigo_gerado = inimigo_gerado;
	}
	public int getInimigo_eliminado() {
		return inimigo_eliminado;
	}
	public void setInimigo_eliminado(int inimigo_eliminado) {
		this.inimigo_eliminado = inimigo_eliminado;
	}
	

}
