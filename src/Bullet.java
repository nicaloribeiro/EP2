import java.awt.Graphics;
import java.awt.Rectangle;

public class Bullet extends GameObject implements EntityA{
	
	private Texturas tex;
	private Game game;
	
	public Bullet( double x, double y, Texturas tex, Game game){
		super(x, y);
		this.tex = tex;
		this.game = game;
		
		
	}
	public void tick(){
		y -= 8;
		
		if(Physics.Collision(this, game.eb))
		{
			System.out.println("COLISAO DETECTADA");
			
		}
	}
	
	public Rectangle getBounds(){
		return new Rectangle((int)x, (int)y, 32, 32);
	}
	
	public void render(Graphics g){
		g.drawImage(tex.tiro, (int) x, (int) y, null );
	}
	public double getY(){
		return y;
	}
	
	public double getX() {
		return x;
	}

}
